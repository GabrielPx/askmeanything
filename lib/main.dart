import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        home: BallPage(),
      ),
    );

class BallPage extends StatelessWidget {
  const BallPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Me Pergunte Qualquer Coisa'),
        backgroundColor: Colors.blueAccent,
      ),
      body: Ball(),
    );
  }
}

class Ball extends StatefulWidget {
  const Ball({Key key}) : super(key: key);


  @override
  _BallState createState() => _BallState();
}

class _BallState extends State<Ball> {

  int imageNumber = 1;

  randomNumber(){
    setState(() {
      imageNumber = Random().nextInt(5) + 1;
      print(imageNumber);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      color: Colors.grey[400],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: GestureDetector(
              child: Image.asset('images/ball$imageNumber.png'),
              onTap: randomNumber,
            ),
          ),
        ],
      ),
    );
  }
}
